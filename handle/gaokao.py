
import json
import re
import os


class baidugaokao():
    def __init__(self):
        self.save_path = "../data/百度高考/"
        self.handle_path = "../handle_data/百度高考/"

    def gaokao(self):
        f = open(self.handle_path +"name.txt",'w',encoding="utf8")
        f_sudy = open(self.handle_path + "study.txt", 'w', encoding="utf8")
        f_zhuan = open(self.handle_path + "impress.txt", 'w', encoding="utf8")
        f_code =open(self.handle_path + "code.txt", 'w', encoding="utf8")
        f_xueke = open(self.handle_path + "xueke.txt", 'w', encoding="utf8")
        f_do_what = open(self.handle_path + "do_what.txt", 'w', encoding="utf8")
        f_school = open(self.handle_path + "school.txt", 'w', encoding="utf8")
        school=[]
        id_name = []
        for file in os.listdir(self.save_path):
            if file.find("school")==-1 and file.find("url")==-1:
                with open(self.save_path + file, encoding='utf8') as data_json:
                    data_json = data_json.read()
                    data_json = json.loads(json.loads(json.dumps(data_json).replace("\n",'')))
                if data_json['content']:
                    print(file, '-----------',re.split("<p>(.*?)</p>",str(re.sub("[\n\t\r]","",data_json['content']))))
                id_name.append(data_json['name'])
                f.write(data_json['name']+'\t'+data_json['degree']+'\t'+data_json['limit_year']+'\n')
                f_sudy.write(data_json['name'] + '\t' + '\t'.join(re.findall("《(.*?)》", data_json['learn_what'])) + '\n')
                f_zhuan.write(data_json['name']+'\t'+'\t'.join([element['key_word'] for element in data_json['impress']])+'\n')
                f_code.write(data_json['name'] + '\t' + data_json['code']+ '\n')
                for element in data_json['do_what'].split('\n'):
                    element = element.split("：")
                    # print(data_json)
                    f_do_what.write(data_json['name']+'\t'+element[0]+'\t'+'\t'.join([i for i in re.split("[、。；]",element[1]) if i!=''])+ '\n')
                f_xueke.write(data_json['name'] + '\t' + '\t'.join(data_json['sel_adv'].split('/'))+ '\n')
            elif file.find("school")!=-1 and file.find("url")==-1:
                with open(self.save_path + file, encoding='utf8') as data_json:
                    data_json = data_json.read()
                    data_json = json.loads(json.loads(json.dumps(data_json).replace("\n", '')))
                    school_temp = []
                    if data_json:
                        for key,value in data_json.items():
                            for element in value['data']['item']:
                                school_temp.append(element['name'])
                    school.append(list(set(school_temp)))
        for i in range(len(school)):
            f_school.write(id_name[i] + '\t' + '\t'.join(school[i])+'\n')

                            # print(key,value['data'])


baidugaokao().gaokao()