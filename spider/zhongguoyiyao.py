import re
import requests
from bs4 import BeautifulSoup
import json


class zhongGuoYiYao():
    def __init__(self):
        self.main_url = ['http://www.heartabc.com/xxgbdq/xxgnkcjjb/#1', 'http://www.heartabc.com/xxgbdq/xxgnkqtjb/',
                         'http://www.heartabc.com/xxgbdq/xxwkqtjb/', 'http://www.heartabc.com/xxgbdq/xxwkcjjb/']
        self.headers = {
            'Host': 'api.dayi.org.cn:9997',
            'Origin': 'https://www.dayi.org.cn',
            'Sec-Fetch-Dest': 'empty',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-site',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'zh-CN,zh;q=0.9',
            'Connection': 'keep-alive',
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36"}
        self.save_path = "../data/中国医药信息查询平台/"
        self.main_data = {'symptom': 162, 'doctor': 443, 'medical': 937, 'cmedical': 538,
                          'healthProducts': 12,
                          'prescriptions': 831, 'diteGuidelines': 5, 'acupuncture': 67, 'term': 59, 'classics': 25,
                          'ancientPhysicians': 4, 'institution': 319}
        # 'https://api.dayi.org.cn:9997/api/institution/list?pageNo=319'#

    #     "https://api.dayi.org.cn:9997/api/disease/queryDiseaseList?pageNo=2&pageSize=20"

    def get_url(self, type, start):
        return 'https://api.dayi.org.cn:9997/api//' + type + '/list?pageNo=' + start + '&pageSize=20'

    def get_single_url(self, type, id):
        return 'https://api.dayi.org.cn:9997/api/' + type + '/' + id

    def get_page(self):
        for key, value in self.main_data.items():
            content = []
            for i in range(1, value + 1):
                print(i)
                url = self.get_url(key, str(i))
                data = requests.get(url, headers=self.headers).json()
                content.append(data)
            with open(self.save_path + key + '_url.json', "w", encoding="utf8") as dump_f:
                json.dump({"url": content}, dump_f, ensure_ascii=False, indent=2)

    def get_single(self):
        for type, value in self.main_data.items():
            print(type)
            with open(self.save_path + type + '_url.json', encoding='utf8') as data_json:
                data_json = json.load(data_json)['url']
            content = []
            for element in data_json:
                for element_two in element['list']:
                    content_url = self.get_single_url(type, str(element_two['id']))
                    try:
                        data_temp = requests.get(content_url, headers=self.headers).json()
                        content.append(data_temp)
                    except:
                        print(content_url)
            with open(self.save_path + type + '_content.json', "w", encoding="utf8") as dump_f:
                json.dump({"content": content}, dump_f, ensure_ascii=False, indent=2)

    def symptom(self):
        with open(self.save_path + '0ed8e692-ed25-11ea-af79-e7a77a8a0c17.json', encoding='utf8') as data_json:
            data_json = json.load(data_json)['content']
            print(data_json['data'])


print(zhongGuoYiYao().symptom())

