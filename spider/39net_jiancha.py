import re
import requests
from bs4 import BeautifulSoup
import json
import os
import pymongo
import uuid


class spiderCheck39():
    """
    39net检查数据爬取
    """

    def __init__(self):
        # self.myclient = pymongo.MongoClient("")
        # self.mydb = self.myclient['39net']["jiancha"]
        self.headers = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'Accept-encoding': 'gzip, deflate, br',
            'Accept-language': 'zh-CN,zh;q=0.9',
            'Cache-control': 'max-age=0',
            "Host": "jbk.39.net",
"Upgrade-Insecure-Requests": "1",
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36"}
        self.department = {"头部": {"url": "http://jbk.39.net/bw/toubu_t3_p", "page": 32},
                           "颈部": {"url": "http://jbk.39.net/bw/jingbu_t3_p", "page": 6},
                           "胸部": {"url": "http://jbk.39.net/bw/xiongbu_t3_p", "page": 19},
                           "腹部": {"url": "http://jbk.39.net/bw/fubu_t3_p", "page": 29},
                           "腰部": {"url": "http://jbk.39.net/bw/yaobu_t3_p", "page": 15},
                           "臀部": {"url": "http://jbk.39.net/bw/tunbu_t3_p", "page": 3},
                           "上肢": {"url": "http://jbk.39.net/bw/shangzhi_t3_p", "page": 7},
                           "下肢": {"url": "http://jbk.39.net/bw/xiazhi_t3_p", "page": 6},
                           "骨": {"url": "http://jbk.39.net/bw/gu_t3_p", "page": 8},
                           "会阴": {"url": "http://jbk.39.net/bw/huiyinbu_t3_p", "page": 1},
                           "男性生殖": {"url": "http://jbk.39.net/bw/nanxingshengzhi_t3_p", "page": 6},
                           "女性生殖": {"url": "http://jbk.39.net/bw/nvxingshengzhi_t3_p", "page": 14},
                           "盆腔": {"url": "http://jbk.39.net/bw/penqiang_t3_p", "page": 5},
                           "全身": {"url": "http://jbk.39.net/bw/quanshen_t3_p", "page": 95},
                           "心理": {"url": "http://jbk.39.net/bw/xinli_t3_p", "page": 1},
                           "背部": {"url": "http://jbk.39.net/bw/beibu_t3_p", "page": 1},
                           "其它": {"url": "http://jbk.39.net/bw/qita_t3_p", "page": 12}}
        self.save_path = "../data/39net/检查1/"

    def page_check(self, url, page):
        """
        获取单个部位单页的检查链接
        :param url: 
        :param page: 
        :return: 
        """
        result = {}
        for i in range(1, page + 1):
            response = requests.get(url + str(i), headers=self.headers)
            soup = BeautifulSoup(response.text, 'html.parser')
            for element_one in soup.find_all(class_='result_item'):
                for children in element_one.find_all('p'):
                    if children['class'] == ['result_item_top_l']:
                        for element_three in children.find_all('a'):
                            result[element_three.get_text()] = element_three['href']
        return result

    def get_all_check(self):
        """
        获取全部检查的链接
        :return: 
        """
        result = {}
        for key, value in self.department.items():
            print(key, value)
            result[key] = self.page_check(value['url'], value['page'])
        with open(self.save_path + 'url.json', "w", encoding="utf8") as dump_f:
            json.dump(result, dump_f, ensure_ascii=False, indent=2)

    def single_data(self, url):
        """
        爬取单页检查内容
        :param url: 
        :return: 
        """
        response = requests.get(url, headers=self.headers)
        soup = BeautifulSoup(response.text, 'html.parser')
        title = ''
        name = ''
        content_data = {}
        for element in soup.find_all(class_='tit clearfix'):
            for element_one in element.find_all("h1"):
                for element_two in element_one.find_all("b"):
                    title = element_two.get_text()
            for element_four in element.find_all("span"):
                name = element_four.get_text()
        content_data['别名'] = name
        content_data['检查名字'] = title
        tag = []
        detail = []
        for element in soup.find_all(class_='ss_det catalogItem'):
            for element_ in element.find_all(class_='tag'):
                for element_one in element_.find_all("span"):
                    for element_two in element_one.find_all("a"):
                        tag.append(element_two.get_text())
            for element__ in element.find_all(class_='des'):  # 简介
                for element_three in element__.find_all(class_='con'):
                    detail.append(element_three.get_text())

            for element___ in element.find_all(class_='infolist'):
                for element_one in element___.find_all('li'):
                    if '<a' in str(element_one):
                        key_ = ''
                        value_ = []
                        for element_two in element_one.find_all('b'):
                            key_ = element_two.get_text()
                        for element_three in element_one.find_all('a'):
                            value_.append([element_three.get_text(), element_three['href']])
                        content_data[key_] = value_
                    else:
                        key_ = ''
                        for element_two in element_one.find_all('b'):
                            key_ = element_two.get_text()
                        value_ = [element_one.get_text()]
                        content_data[key_] = value_
        content_data['标签'] = tag
        content_data['简介'] = detail
        for element in soup.find_all(class_='lbox catalogItem'):
            key = ''
            for children in element.find_all(class_='tit clearfix'):
                key = children.get_text()
            value = []
            if str("text") in str(element):
                text = []
                for children_ in element.find_all(class_='text'):
                    for children__ in children_.find_all('p'):
                        text.append(children__.get_text())
                value = text
            if str("type_table") in str(element):
                table = []
                for element_one in element.find_all(class_='type_table'):
                    for element_two in element_one.find_all(class_='name'):
                        for element_three in element_two.find_all("a"):
                            table.append({"url": element_three['href'], "text": element_three.get_text()})
                value = table
            if str("con clearfix") in str(element):
                table_ = []
                for element_four in element.find_all(class_='con clearfix'):
                    for element_five in element_four.find_all("ul"):
                        for element_six in element_five.find_all("li"):
                            for element_seven in element_six.find_all("a"):
                                table_.append({"url": element_seven['href'], "text": element_seven.get_text()})
                value = table_
            content_data[key] = value
        # print(content_data)
        for element in soup.find_all(class_='rbox'):
            # print(element)
            key = ''
            for element_one in element.find_all(class_='tit clearfix'):
                key = element_one.get_text()
            if key.find("您最近浏览的检查") != -1 or key.find('精彩推荐') != -1:
                continue
            value = []
            for element_two in soup.find_all(class_='listFull item'):
                for element_three in element_two.find_all('li'):
                    temp = []
                    for element_four in element_three.find_all('span'):
                        temp.append(element_four.get_text())
                    for element_five in element_three.find_all('a'):
                        temp.extend([element_five['href'], element_five.get_text()])
                    value.append(temp)
            content_data[key] = value
        return content_data

    def get_all(self):
        """
        获取全部检查信息
        :return: 
        """
        with open(self.save_path + 'url.json', encoding='utf8') as data_json:
            data_json = json.load(data_json)
            for key, value in data_json.items():
                for key_, value_ in value.items():
                    print(key_)
                    data = self.single_data(value_)
                    data['type'] = key
                    data['name'] = key_
                    name1 =str(uuid.uuid1())
                    with open(self.save_path + name1 + '.json', "w",
                              encoding="utf8") as dump_f:
                        json.dump(data, dump_f, ensure_ascii=False, indent=2)

    def re_sub_key(self, data, name):
        return re.sub(name, '', re.sub(('[\n\r\t：]'), '', data))

    def single_handle(self, file):
        """
        单个检查数据清洗
        :param file: 
        :return: 
        """
        print(file)
        with open(file, encoding='utf8') as data_json:
            data_json = json.load(data_json)
        temp = {}
        for key, value in data_json.items():
            key_ = self.re_sub_key(key, data_json['检查名字'])
            if isinstance(value, list):
                if key_.find('部位') != -1:
                    position = []
                    for element in value:
                        if element:
                            position.append(element[0])
                    temp['部位'] = position
                elif key_.find('科室') != -1:
                    depart = [re.sub(('[\n \r \t  ：\xa0\u3000]'), '', element) for element in
                              ''.join(value).replace(key, '').split("\n")
                              if element != '']
                    temp['科室'] = [element for element in depart if element != '']
                elif key_.find('空腹检查') != -1:
                    temp['空腹检查'] = re.sub(('[\n \r \t  ：\xa0\u3000]'), '', ''.join(value).replace(key, ''))
                elif key_.find('简介') != -1:
                    temp['简介'] = re.sub(('[\n \r \t  ：\xa0\u3000]'), '', ''.join(value).replace(key, ''))
                elif key_.find('相关') != -1 or key_.find('同类') != -1 or key_.find(
                        '所属') != -1 or key_ == '指标解读' or key_ == '包含项目':
                    if key_ == '指标解读':
                        continue
                    relevant_value = []
                    for element in value:
                        if len(element) == 2:
                            if isinstance(element, dict):
                                relevant_value.append(element['text'])
                            else:
                                relevant_value.append(element[0])
                        else:
                            relevant_value.append(element[2])
                    temp[key_] = relevant_value
                else:
                    if len(value) > 1:
                        if value[1] in value[0]:
                            del value[0]
                        temp[key_] = [re.sub('[\n \r \t  ：\xa0\u3000]', '', element) for element in value]
                    else:
                        if key_.find('医院参考价') == -1:
                            # print(value,key_)
                            temp[key_] = [re.sub(('[\n \r \t  ：\xa0\u3000]'), '', element) for element in value]

            elif key_.find('type') != -1:
                temp['部位大类'] = value
            elif key_.find('别名') != -1:
                temp['别名'] = [element for element in re.split('[，,]',
                                                              value.replace(key, '').replace('（', '').replace('）',
                                                                                                              '').replace(
                                                                  '：', '')) if element != '']
            else:
                temp[key_] = value
        return temp

    def handle_all(self):
        """
        全部数据入库
        :return: 
        """
        for file in os.listdir(self.save_path):
            try:
                result = self.single_handle(self.save_path + file)
                # res = self.mydb.insert_one(result)
                print(result)
            except:
                pass


if __name__ == '__main__':
    spiderCheck39().handle_all()
