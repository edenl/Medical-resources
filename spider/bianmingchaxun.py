import re
import requests
from bs4 import BeautifulSoup
import json
import os
import pymongo
import uuid


class spiderBianMingChaXun():
    """
    便民查询网数据爬取
    """

    def __init__(self):
        self.save_home = "../data/便民查询网/"
        self.main_url = 'https://yingyang.51240.com/'
        self.headers = {
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'zh-CN,zh;q=0.9',
            'cache-control': 'no-cache',
            'pragma': 'no-cache',
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36"}

    def single(self, url, name, type):
        result = {}
        result['url'] = url
        result['名字'] = name
        result['类别'] = type
        response = requests.get(url, headers=self.headers)
        soup = BeautifulSoup(response.text, 'lxml')
        key_ = []
        value_ = []
        for element in soup.find_all(class_='yingyang wkbx'):
            for element_one in element.find_all('tr'):
                for element_two in element_one.find_all(['th', 'td']):
                    if element_two.name == 'th':
                        key_.append(element_two.get_text())
                    else:
                        value_.append(element_two.get_text())
        result['营养成分'] = dict(zip(key_, value_))
        return result

    def get_type_url(self, url):
        result = []
        response = requests.get(url, headers=self.headers)
        soup = BeautifulSoup(response.text, 'lxml')
        for element in soup.find_all(class_='list'):
            for element_one in element.find_all('li'):
                for element_two in element_one.find_all('a'):
                    temp = {}
                    temp['名字'] = element_two.get_text()
                    temp['url'] = element_two['href']
                    result.append(temp)
        return result

    def get_type(self):
        result = []
        response = requests.get(self.main_url, headers=self.headers)
        soup = BeautifulSoup(response.text, 'lxml')
        for element in soup.find_all(class_='xiaoshuomingkuang_neirong'):
            for element_one in element.find_all('p'):
                if '食物分类：' in str(element_one):
                    for element_two in element_one.find_all('a'):
                        temp = {}
                        temp['类别'] = element_two.get_text()
                        temp['url'] = element_two['href']
                        result.append(temp)
        return result

    def get_data(self):
        result = []
        for element in self.get_type():
            url_type = 'https://yingyang.51240.com' + element['url']
            type = element['类别']
            for element_one in self.get_type_url(url_type):
                name = element_one['名字']
                print(name)
                name_url = 'https://yingyang.51240.com' + element_one['url']
                result.append(self.single(name_url, name, type))
        with open(self.save_home + '便民查询网_.json', "w", encoding="utf8") as dump_f:
            json.dump({"content": result}, dump_f, ensure_ascii=False, indent=2)


if __name__ == '__main__':
    spiderBianMingChaXun().get_data()
