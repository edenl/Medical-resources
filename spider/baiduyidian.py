import re
import requests
from bs4 import BeautifulSoup
import json
import os
import time
import pymysql
import uuid


class spiderBaiDuYiDian():
    def __init__(self):
        self.main_data = [
            {'name': '头部', 'url': 'https://expert.baidu.com/med/page/jp/yidian/getdiseaselist?type=2&item=头部'},
            {'name': '颈部', 'url': 'https://expert.baidu.com/med/page/jp/yidian/getdiseaselist?type=2&item=颈部'},
            {'name': '胸部', 'url': 'https://expert.baidu.com/med/page/jp/yidian/getdiseaselist?type=2&item=胸部'},
            {'name': '腹部', 'url': 'https://expert.baidu.com/med/page/jp/yidian/getdiseaselist?type=2&item=腹部'},
            {'name': '腰部', 'url': 'https://expert.baidu.com/med/page/jp/yidian/getdiseaselist?type=2&item=腰部'},
            {'name': '背部', 'url': 'https://expert.baidu.com/med/page/jp/yidian/getdiseaselist?type=2&item=背部'},
            {'name': '骨盆区', 'url': 'https://expert.baidu.com/med/page/jp/yidian/getdiseaselist?type=2&item=骨盆区'},
            {'name': '生殖部位', 'url': 'https://expert.baidu.com/med/page/jp/yidian/getdiseaselist?type=2&item=生殖部位'},
            {'name': '四肢', 'url': 'https://expert.baidu.com/med/page/jp/yidian/getdiseaselist?type=2&item=四肢'},
            {'name': '皮肤', 'url': 'https://expert.baidu.com/med/page/jp/yidian/getdiseaselist?type=2&item=皮肤'},
            {'name': '骨骼肌肉', 'url': 'https://expert.baidu.com/med/page/jp/yidian/getdiseaselist?type=2&item=骨骼肌肉'},
            {'name': '血液', 'url': 'https://expert.baidu.com/med/page/jp/yidian/getdiseaselist?type=2&item=血液'},
            {'name': '全身', 'url': 'https://expert.baidu.com/med/page/jp/yidian/getdiseaselist?type=2&item=全身'}]
        self.headers = {
            'Host': 'www.baidu.com',
            'Referer': 'https://expert.baidu.com/med/template/',
            'Sec-Fetch-Dest': 'document',
            'Sec-Fetch-Mode': 'navigate',
            'Sec-Fetch-Site': 'same-origin',
            'Sec-Fetch-User': '?1',
            'Upgrade-Insecure-Requests': '1',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'zh-CN,zh;q=0.9',
            'Cache-Control': 'max-age=0',
            'Connection': 'keep-alive',
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36"}
        self.main_headers = {'Host': 'expert.baidu.com',
                             'Referer': 'https://expert.baidu.com/med/template/',
                             'Sec-Fetch-Dest': 'empty',
                             'Sec-Fetch-Mode': 'cors',
                             'Sec-Fetch-Site': 'same-origin',
                             'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36',
                             'Accept': '*/*',
                             'Accept-Encoding': 'gzip, deflate, br',
                             'Accept-Language': 'zh-CN,zh;q=0.9',
                             'Connection': 'keep-alive'

                             }
        self.save_path = "../data/百度医典/"

    def get_single(self, url):
        response = requests.get(
            url,
            headers=self.headers)
        soup = BeautifulSoup(response.text, 'html.parser')
        response_list = {}
        for element in soup.find_all(class_='health-dict__disease__header__info--wrapper'):
            for element_one in element.find_all(class_='health-dict__disease__header__info__title'):
                response_list['name'] = element_one.get_text()
            for element_two in element.find_all(class_='health-dict__disease__header__info__text'):
                detail = element_two.get_text().split('：')
                if len(detail) > 1:
                    response_list[detail[0]] = detail[1]
        res_data = []
        my_key = []
        for element in soup.find_all(id='richTextContainer'):

            for element_one in element.find_all(
                    class_='health-dict__overview__text health-dict__overview__texts__text'):
                # res_dict ={}
                # res_key =''
                for element_two in element_one.find_all(class_='health-dict__overview__text__level1-tag__title'):
                    my_key.append(element_two.get_text())
                res_list = []
                for element_three in element_one.find_all(class_='health-dict__overview__text__level1-tag c-gap-top'):
                    temp_dict = {}
                    temp_key = ''
                    for element_four in element_three.find_all(class_='health-dict__overview__text__level1-tag__texts'):
                        for element_five in element_four.find_all('li'):
                            temp_key = element_five.get_text()
                    all_temp = {}
                    one_result = {}
                    table = {}
                    table_list = []
                    ul_data = []
                    for element_six in soup.find_all(class_='health-dict__html'):
                        for element_seven in element_six.find_all('div'):
                            tag = []
                            for count, element_eight in enumerate(
                                    element_seven.find_all(['h2', 'h3', 'h4', 'p', 'ul', 'table'])):
                                if element_eight.name != 'p':
                                    key_ = element_eight.get_text() + '_' + str(element_eight.name)
                                    one_result[key_] = tag
                                    tag = []
                                else:
                                    tag.append(element_eight.get_text())
                                if count == len(element_seven.find_all(['h2', 'h3', 'h4', 'p', 'ul', 'table'])) - 1:
                                    one_result['count'] = (element_eight.get_text())
                            for element_nine in element_seven.find_all('table'):
                                temp_table = {}
                                td = []
                                th = []
                                for element_ten in element_nine.find_all('tr'):
                                    for element_eleven in element_ten.find_all('th'):
                                        th.append(element_eleven.get_text())
                                    for element_twelve in element_ten.find_all('td'):
                                        td.append(element_twelve.get_text())
                                temp_table['th'] = th
                                temp_table['td'] = td
                                table_list.append(temp_table)
                            temp_ul = []
                            for children_data in element_seven.find_all('ul'):
                                for element_data in children_data.find_all('li'):
                                    temp_ul.append(element_data.get_text())
                            if temp_ul != []:
                                ul_data.append(temp_ul)

                    table['table'] = table_list
                    key_ = [key for key, value in one_result.items()]
                    key_.remove('count')
                    value_ = [value for key, value in one_result.items()]
                    del value_[0]
                    all_temp['data'] = dict(zip(key_, value_))
                    all_temp['table'] = table
                    all_temp['ul'] = ul_data
                    all_temp['my_key'] = my_key
                    temp_dict[temp_key] = all_temp
                    res_list.append(all_temp)
                # res_dict[res_key]=res_list
                # res_data.append(res_dict)
                response_list['data'] = res_list
        return response_list
        # print(json.dumps(response_list, ensure_ascii=False, indent=2))

    def get_url(self):
        for element in self.main_data:
            name = element['name']
            print(name)
            data = []
            for i in range(300):
                url = element['url'] + '&pn=' + str(
                    i * 30) + '&rn=30&referlid=3038674177&applid=3038684582&lid=5816110563'
                temp = json.dumps(requests.get(url, headers=self.main_headers).json(), ensure_ascii=False, indent=2)
                # print(temp)
                if json.loads(temp)['data']['itemDiseaseList'] == []:
                    break
                data.append(temp)
            with open(self.save_path + name + '_url.json', "w", encoding="utf8") as dump_f:
                json.dump({"url": data}, dump_f, ensure_ascii=False, indent=2)

    def spider_all(self):
        for file in os.listdir(self.save_path):
            with open(self.save_path + file, encoding='utf8') as data_json:
                data_json = json.load(data_json)['url']
                for element in data_json:
                    element = element.replace("\n", '').replace("\\", '')
                    for children in json.loads(element)['data']['itemDiseaseList']:
                        url = children['jumpUrl']
                        id = url[url.find('contentid='):].replace('contentid=', '')
                        url = 'https://www.baidu.com/bh/dict/' + id + '?tab=%E6%A6%82%E8%BF%B0&contentid=' + id + '&from=dicta'
                        try:
                            name = str(uuid.uuid1())
                            with open(self.save_path + name + '.json', "w", encoding="utf8") as dump_f:
                                json.dump(self.get_single(url), dump_f, ensure_ascii=False, indent=2)
                        except:
                            print(url)

    def symptom(self):
        import xlwt
        # 创建一个workbook 设置编码
        workbook = xlwt.Workbook(encoding='utf-8')
        # 创建一个worksheet
        worksheet = workbook.add_sheet('sheet')

        # 写入excel
        # 参数对应 行, 列, 值

        con =[]
        name =[]
        for file in os.listdir(self.save_path):
            if file.find('url')==-1:
                with open(self.save_path + file, encoding='utf8') as data_json:
                    data_json = json.load(data_json)

                    for key,value in data_json['data'][0]['data'].items():
                        if key.find('典型症状')!=-1:
                            con.append(value)
                            name.append(data_json['name'])
        for j in range(len(con)):
            worksheet.write(j, 0, con[j])
            worksheet.write(j, 1, name[j])
        workbook.save('百度.xls')



spiderBaiDuYiDian().symptom()
