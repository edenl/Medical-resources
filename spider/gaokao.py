import requests
import json
import re


class gaoKao():
    """
    掌上高考医学专业收集
    """

    def __init__(self):
        self.headers = {
            "Origin": 'https://gkcx.eol.cn',
            "Sec-Fetch-Dest": 'empty',
            "Content-Type": "application/json;charset=UTF-8",
            'Connection': 'keep-alive',
            'Accept-Language': 'zh-CN,zh;q=0.9',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept': 'application/json, text/plain, */*',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-site',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36'}
        self.save_path = "../data/百度高考/"

    def get_url(self):
        """
        获取医学专业
        :return:
        """
        headers = self.headers
        headers['Referer'] = 'https://gkcx.eol.cn/special?zytype=%E5%8C%BB%E5%AD%A6'
        headers["Host"] = 'api.eol.cn'
        content = []
        for i in range(3):
            data = {"access_token": "", "keyword": "", "level1": "", "level2": 12, "page": i + 1, "size": 20,
                    "uri": "apidata/api/gk/special/lists"}
            result = requests.post(
                "https://api.eol.cn/gkcx/api/?access_token=&keyword=&level1=&level2=12&page=1&signsafe=&size=20&uri=apidata/api/gk/special/lists",
                headers=headers, data=data).json()
            for element in result['data']['item']:
                temp = {}
                temp['名字'] = element['name']
                temp['学位'] = element['degree']
                temp['归属'] = element['level3_name']
                temp['学位获得时间'] = element['limit_year']
                temp['学校类别'] = element['level1_name']
                temp['url'] = 'https://gkcx.eol.cn/special/' + str(element['special_id'])
                content.append(temp)
        with open(self.save_path + 'url.json', "w",
                  encoding="utf8") as dump_f:
            json.dump({"url": content}, dump_f, ensure_ascii=False, indent=2)

    def single(self, url):
        """
        单个医学专业数据收集
        :param url: 医学专业url
        :return:
        """
        headers = self.headers
        headers['Referer'] = url
        headers["Host"] = 'static-data.eol.cn'
        id = re.findall("\d+", url)[0]
        result = requests.get(
            'https://static-data.eol.cn/www/special/' + id + '/pc_special_detail.json',
            headers=headers).json()
        with open(self.save_path + id + '.json', "w",
                  encoding="utf8") as dump_f:
            json.dump(result, dump_f, ensure_ascii=False, indent=2)

    def get_all(self):
        """
        获取全部的医学专业数据
        :return:
        """
        with open(self.save_path + 'url.json', encoding='utf8') as data_json:
            data_json = json.load(data_json)
        for element in data_json['url']:
            try:
                self.single(element['url'])
            except:
                print(element['url'] + 'fail')

    def get_single_school(self, url):
        """
        获取单个医学专业所对应的学校dict
        :param url:
        :return:
        """
        headers = self.headers
        id = re.findall("\d+", url)[0]
        headers['Referer'] = 'https://gkcx.eol.cn/specials/school/' + str(id)
        headers["Host"] = 'api.eol.cn'
        headers["Content-Length"] = '174'
        content = {}
        for i in range(1, 10):
            data = {"access_token": "", "keyword": "", "page": str(i), "province_id": "", "request_type": "1",
                    "school_type": "", 'signsafe': '',
                    "size": "20", "special_id": str(id), "type": "", "uri": "apidata/api/gk/schoolSpecial/lists"}
            data_url = 'https://api.eol.cn/gkcx/api/?'
            for key, value in data.items():
                data_url = data_url + key + '=' + value + '&'
            data_url = data_url.strip("&")
            print(data_url)
            del data['signsafe']
            result = requests.post(data_url, headers=headers, data=data).json()
            if len(result['data']['item']) == 0:
                break
            else:
                content[str(i)] = result
            # print(json.dumps(result, ensure_ascii=False, indent=2))
        with open(self.save_path + id + '_school_.json', "w",
                  encoding="utf8") as dump_f:
            json.dump(content, dump_f, ensure_ascii=False, indent=2)

    def get_all_school(self):
        """
        获取全部医学专业对应的学校dict
        :return:
        """
        with open(self.save_path + 'url.json', encoding='utf8') as data_json:
            data_json = json.load(data_json)
        for element in data_json['url']:
            try:
                self.get_single_school(element['url'])
            except:
                print(element['url'] + 'fail')


if __name__ == '__main__':
    gaoKao().get_all_school()
